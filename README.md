# Card_studentesc

---

## Descriere

Sistem de plati online pentru studenti

---

## Componente

1. Site web:
	* HTML / CSS
	* Javascript
1. Oracle APEX:
	* Baza de date
	* Servicii REST pentru accesare
	* Tutorial REST (optional)
1. Proiectare baza de date:
	* Oracle Data Modeler
	* Entity relationship